# TODO App backend

## Business

### User stories

Người dùng có thể tạo nhiều todo item
Người dùng có thể mark done 1 hoặc nhiều todo item
Người dùng có thể xem toàn bộ todo (Bao gồm đã hoàn thành và chưa hoàn thành)
Người dùng có thể filter những todo đã hoàn thành
Người dùng có thể filter những todo chưa hoàn thành
Người dùng có thể xóa các todo
Người dùng có thể xóa toàn bộ những todo đã hoàn thành

## Analyse

### Build APIs (RESTful API)

- Create todo (create single todo for each time) ('/api/createTodo')
- Update Todo & mark this todo as completed ('/api/updateTodo')
- Get all Todo ('/api/todos')
- Get all completed todos ('/api/todos' with params 'completed:true')
- Get all activated todos ('/api/todos' with params 'completed:false')
- Delete Todo ('/api/deleteTodo)
- Clear todo completed ('/api/clearTodo')
