const express = require('express');
const router = express.Router();
const { createTodo, updateTodo, getTodoList, deleteTodo, clearTodo } = require('../controllers/todoController');

// - Create todo (create single todo for each time) ('/api/createTodo')
// - Update Todo & mark this todo as completed ('/api/updateTodo')
// - Get all Todo ('/api/todos')
// - Get all completed todos ('/api/todos' with params 'completed:true')
// - Get all activated todos ('/api/todos' with params 'completed:false')
// - Delete Todo ('/api/deleteTodo')
// - Clear todo completed ('/api/clearTodo')

router.post('/createTodo', createTodo);
router.post('/updateTodo', updateTodo);
router.post('/todos', getTodoList);
router.post('/deleteTodo', deleteTodo);
router.post('/clearTodo', clearTodo);

module.exports = router;
