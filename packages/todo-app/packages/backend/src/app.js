const express = require('express');
const router = require('./router');
var cors = require('cors');
const bodyParser = require('body-parser');
const allRoutes = require('express-list-endpoints');

const app = express();
const port = 3000;

app.use('/api', router);
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
  console.log('Registered routes: ');
  console.log(allRoutes(app));
});
