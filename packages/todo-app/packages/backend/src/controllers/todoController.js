// - Create todo (create single todo for each time) ('/api/createTodo')
// - Update Todo & mark this todo as completed ('/api/updateTodo')
// - Get all Todo ('/api/todos')
// - Get all completed todos ('/api/todos' with params 'completed:true')
// - Get all activated todos ('/api/todos' with params 'completed:false')
// - Delete Todo ('/api/deleteTodo')
// - Clear todo completed ('/api/clearTodo')

const todoModel = require('../models/todo.js');

//title, completed
exports.createTodo = (req, res) => {
  const todo = req.body;
  const inserted = todoModel.insert(todo);
  res.json(inserted);
  //
};
exports.updateTodo = (req, res) => {
  res.json('Hello');
};
exports.getTodoList = (req, res) => {};
exports.deleteTodo = (req, res) => {};
exports.clearTodo = (req, res) => {};
