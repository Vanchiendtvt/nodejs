let todos = [];

/**insert TOdo
 * @todo{id, title, completed}
 */

exports.insert = (todo) => {
  const tobeTodo = { ...todo, completed: false };
  todos.push(tobeTodo);
  return tobeTodo;
};

/**Update todo by ID
 * @param {todo: Todo}
 * @return todo
 */
exports.updateById = (todo) => {
  let index = todos.findIndex((t) => t.id === todo.id); //todo or undefined
  if (index != -1) {
    todos[index] = { ...todos[index], ...todo };
    return todos[index];
  } else {
    return false;
  }
};

exports.deleteById = (id) => {
  const todoIdx = todos.findIndex((todo) => todo.id === id); // indexof or -1
  if (todoIdx === -1) {
    return false;
  }
  todos.splice(todoIdx, 1); //delete call DB
  return true;
};

/**
 * find all todos
 * @params {completed}
 * @retturn {list}
 */
exports.findAll = (params) => {
  if (!params) {
    return todos;
  } else {
    const { completed } = params;
    return todos.filter((p) => p.completed);
  }
};

exports.todos = todos;
