const { insert, updateById, deleteById, findAll, todos } = require('../todo');

describe('Test todo model', () => {
  it('Shoudl create todo & return inserted todo', () => {
    const inserted = insert({ id: 1, title: 'first todo' });
    expect(inserted).toEqual({ id: 1, title: 'first todo', completed: false });
    expect(todos.length).toEqual(1);
    expect(todos[0]).toEqual({ id: 1, title: 'first todo', completed: false });
  });

  it('Should update todo & return updated one', () => {
    const updated = updateById({ id: 1, title: 'second todo', completed: true });
    expect(updated.id).toEqual(1);
    expect(updated.title).toEqual('second todo');
    expect(updated.completed).toEqual(true);
    expect(todos[0]).toEqual(updated);
    expect.assertions(4);
  });

  it('Should return false if could not found todo item', () => {
    const updated = updateById({ id: 3, title: 'second todo' });
    expect.assertions(1);
    expect(updated).toEqual(false);
  });

  it('Should deleted item', () => {
    const deletedFail = deleteById(2);
    expect(deletedFail).toEqual(false);
    expect(todos.length).toEqual(1);
    const deleted = deleteById(1);
    expect(deleted).toEqual(true);
    expect(todos.length).toEqual(0);
    expect.assertions(4);
  });

  it('Should found all todos or found todo with param', () => {
    insert({ id: 1, title: 'first todo' });
    insert({ id: 2, title: 'second todo' });
    insert({ id: 3, title: 'third todo' });
    insert({ id: 4, title: 'fourth todo' });
    insert({ id: 5, title: 'fifth todo' });
    updateById({ id: 3, completed: true });
    updateById({ id: 4, completed: true });
    const allTodos = findAll();
    expect(allTodos.length).toEqual(5);
    expect(todos).toEqual(allTodos);
    const completedTodos = findAll('completed');
    expect(completedTodos.length).toEqual(2);
    expect(todos[2]).toEqual(completedTodos[0]);
    expect(todos[3]).toEqual(completedTodos[1]);
    expect.assertions(5);
  });
});
